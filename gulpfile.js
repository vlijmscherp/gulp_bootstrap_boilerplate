/*
MIZO Added:
add NPM bootstrap:
http://treyhunner.com/2015/02/creating-a-custom-bootstrap-build/

manualy copy _variables to modules folder after init

*/

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    header  = require('gulp-header'),
    rename = require('gulp-rename'),
    cssnano = require('gulp-cssnano'),
    concat = require('gulp-concat'),
    package = require('./package.json');

var config = {
        bootstrapDir: './bower_components/bootstrap-sass',
        publicDir: './public',
    };

var banner = [
  '/*!\n' +
  ' * <%= package.name %>\n' +
  ' * <%= package.title %>\n' +
  ' * <%= package.url %>\n' +
  ' * @author <%= package.author %>\n' +
  ' * @version <%= package.version %>\n' +
  ' * Copyright ' + new Date().getFullYear() + '. <%= package.license %> licensed.\n' +
  ' */',
  '\n'
].join('');

gulp.task('bowerscripts', function() {
  return gulp.src([
      'bower_components/bootstrap-sass/javascripts/bootstrap.js',
      'bower_components/owl.carousel/dist/owl.carousel.js',
      'bower_components/sidr/dist/jquery.sidr.js'
    ])
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./src/js/'));
});



gulp.task('css', function () {
    return gulp.src('src/scss/style.scss')
    .pipe(sass({
        includePaths: [
            config.bootstrapDir + '/assets/stylesheets',
            './bower_components/owl.carousel/src/scss'
        ],
    }).on('error', sass.logError))
    .pipe(autoprefixer('last 4 version'))
    .pipe(gulp.dest(config.publicDir+'/assets/css'))
    .pipe(cssnano())
    .pipe(rename({ suffix: '.min' }))
    .pipe(header(banner, { package : package }))
    .pipe(gulp.dest(config.publicDir+'/assets/css'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('fonts', function() {
    return gulp.src(config.bootstrapDir + '/assets/fonts/**/*')
    .pipe(gulp.dest(config.publicDir + '/assets//fonts'));
});

gulp.task('js',function(){
  gulp.src('src/js/*.js')
    .pipe(header(banner, { package : package }))
    .pipe(concat('site_scripts.js'))
    .pipe(gulp.dest(config.publicDir+'/assets/js'))
    .pipe(uglify())
    .pipe(header(banner, { package : package }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(config.publicDir+'/assets/js'))
    .pipe(browserSync.reload({stream:true, once: true}));
});

gulp.task('browser-sync', function() {
    browserSync.init(null, {
        server: {
            baseDir: config.publicDir
        }
    });
});
gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('default', ['bowerscripts','fonts','css', 'js', 'browser-sync'], function () {
    gulp.watch("src/scss/*/*.scss", ['css']);
    gulp.watch("src/js/*.js", ['js']);
    gulp.watch(config.publicDir+"/*.html", ['bs-reload']);
});
