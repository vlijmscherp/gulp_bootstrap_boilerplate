# Vlijmscherp Boilerplate

Boilerplate contains:
- (Bower) Bootstrap SASS
- (Bower) Owl Carousel 2
- (Bower) SIDR sidemenu

Notes:
Inside the /public folder use the .gitify file (and Gitify) to install MODX into the /public folder. All MODX files are excluded from GIT. Keep it this way. We only need to version controll the /assets/ folder (assets/components/ is excluded). The /assets/ folder will contain all snippets, chunks, css, js, etc.
By using the MODX ElementHelper extra, we let MODX create and build all elements from the /assets/elements folder.

The Gulp build-process creates .css and .js into the public/assets folder.
Please make sure to use this build-process otherwise changes can be overwritten.

The .gitify file contains almost no configuration. The most important part is the -packages part. This part defines which packeges/extra's should be installed. These can be installed manually from MODX if you like.

Gitify usage:
https://docs.modmore.com/en/Open_Source/Gitify/Installing_a_Gitify_Project.html

We use Gitify to install MODX and to install all needed extra's. This can be done manually if you like, we just like Gitify better. Gitify can also be used to export all resources, snippets, etc. to json files. The exported files can be pushed to the repo and used for re-creating all assets on live/staging.




## License

#### The MIT License (MIT)

Copyright (c) Vlijmscherp

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.